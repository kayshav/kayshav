import bottle
import MySQLdb as mdb
import sys
from cork import Cork
from cork.backends import SqlAlchemyBackend
from beaker.middleware import SessionMiddleware

sa = SqlAlchemyBackend('sqlite:///:memory:', initialize=True)
#con = mdb.connect('localhost', 'testuser', 'tfowifd', 'testdb');
#with con:
    #cur = con.cursor()
    #cur.execute("DROP TABLE IF EXISTS Users")
    #cur.execute("CREATE TABLE Users(Id INT PRIMARY KEY AUTO_INCREMENT,\
                #Name VARCHAR(25),\ Role VARCHAR(25), \ Username VARCHAR(25), \ Email VARCHAR(25)")
    #cur.execute("INSERT INTO Users(Name) VALUES('Kayshav Prakash') ")
    #cur.execute("INSERT INTO Users(Role) VALUES('Admin') ")
    #cur.execute("INSERT INTO Users(Username) VALUES('kayshav') ")
    #cur.execute("INSERT INTO Users(Email) VALUES('kayshav.prakash@gmail.com') ")
aaa = Cork(backend=sa, smtp_url= 'kayshav.prakash@gmail.com:kayshav2000@smtp.gmail.com'
)
authorize = aaa.make_auth_decorator(fail_redirect="/login", role="user")


app = bottle.default_app()
session_opts = {
    'session.cookie_expires': True,
    'session.encrypt_key': 'please use a random key and keep it secret!',
    'session.httponly': True,
    'session.timeout': 3600 * 24, # 1 day
    'session.type': 'cookie',
    'session.validate_key': True,
}
app = SessionMiddleware(app, session_opts)

def post_get(name, default=''):
    return bottle.request.POST.get(name, default).strip()
@bottle.route('/')
@bottle.route('/login')
def login():
    return '''
         <form action="/login" method="post">
         Username: <input name="username" type="text" />
        Password: <input name="password" type="password" />
        <input value="Login" type="submit" />
    </form>
'''
@bottle.post('/login')
@bottle.post('/')
def login():
    """Authenticate users"""
    username = post_get('username')
    password = post_get('password')
    aaa.login(username, password, success_redirect='/home', fail_redirect='/login')
@bottle.route('/adminlogin')
def adminlogin():
    return'''
         <form action="/login" method="post">
         Username: <input name="adminusername" type="text" />
        Password: <input name="adminpassword" type="password" />
        <input value="Login" type="submit" />
    </form>
'''
@bottle.post('/adminlogin')
def adminlogin():
    username = post_get('adminusername')
    password = post_get('adminpassword')

    aaa.login(username, password, success_redirect='/home', fail_redirect='/login')
@bottle.route('/home')
def home():
    return """ <html>
<head>
	<title></title>
</head>
<body>
<h2 style="text-align: center;"><span style="font-size:72px;">Welcome!</span></h2>
</body>
</html>
"""
@bottle.route('/create_user')
@authorize(role = 'admin', fail_redirect= '/sorry')
def create_user():
    return """
    <html>
<head>
	<title></title>
</head>
<body>
<form action="/login" method="post">
<p style="text-align: center;">&nbsp;</p>

<p style="text-align: center;"><br />
<span style="font-size:36px;">Creat</span><span style="font-size:36px;">e New Account</span></p>

<p style="text-align: center;">Email</p>

<p style="text-align: center;"><input maxlength="50" name="Email" size="12" type="email" /></p>

<p style="text-align: center;">Username</p>

<p style="text-align: center;"><input maxlength="50" name="Username" size="12" type="text" /></p>


<p style="text-align: center;">Role</p>

<p style="text-align: center;"><input maxlength="50" name="Role" size="12" type="text" /></p>

<p style="text-align: center;">Name</p>

<p style="text-align: center;"><input maxlength="50" name="Name" size="12" type="text" /></p>

<p style="text-align: center;"><input name="Submit" type="submit" value="Submit" /></p>
</body>
</form>
</html>

    """

@bottle.post('/create_user')
@authorize(role = 'admin', fail_redirect= '/sorry')
def create_user():
    try:
        aaa.create_user(post_get("Username"), post_get('Role'), 'tfowifd')
        aaa.send_password_reset_email(
        username=post_get('Username'),
        email_addr=post_get('Email')
    )
        print post_get("Username")
        return dict(ok=True, msg='Done')
    except Exception, e:
        return dict(ok=False, msg=e.message)



@bottle.route('/sorry')
def sorry():
    return """
    <html>
<head>
	<title></title>
</head>
<body>
<p><span style="font-size:36px;">Sorry! You Do Not Have Permission</span></p>

<p><span style="font-size:36px;">To View this Page!</span></p>
</body>
</html>

    """
@bottle.route('/create_object')
def create_object():
    return '''
    <html>
<head>
	<title></title>
</head>
<body>
<p style="text-align: center;"><span style="font-size:36px;">Create New Object</span></p>

<p style="text-align: center;"><span style="font-size:26px;">Name</span></p>

<p style="text-align: center;"><input maxlength="50" name="Name" size="12" type="text" /></p>

<p style="text-align: center;"><span style="font-size:26px;">Description</span></p>

<p style="text-align: center;"><span style="font-size:26px;"><textarea cols="26" name="Description" rows="6"></textarea></span></p>

<p style="text-align: center;"><span style="font-size:26px;">List of Materials</span></p>

<p style="text-align: center;"><span style="font-size:26px;"><textarea cols="26" name="ListofMaterials" rows="6"></textarea></span></p>

<p style="text-align: center;"><span style="font-size:26px;">Add an Image</span></p>

<form action="what-to-do-next.php" enctype="multipart/form-data" method="post">
<p style="text-align: center;"><input maxlength="25" name="my-file" size="50" type="file" />&nbsp;&nbsp;<br />
<input name="=image" type="submit" value="Upload!" /></p>
</form>
</body>
</html>

    '''
@bottle.post('/create_object')
def create_object():

    return
bottle.run(app=app, quiet=False, reloader=True)