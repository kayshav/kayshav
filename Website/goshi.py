import bottle
from bottle import request
import MySQLdb as mdb
import xlrd
import os
from string import punctuation as punct
import sys
from cork import Cork
from cork.backends import SqlAlchemyBackend
import datetime
from beaker.middleware import SessionMiddleware

app = bottle.default_app()
session_opts = {
    'session.cookie_expires': True,
    'session.encrypt_key': 'please use a random key and keep it secret!',
    'session.httponly': True,
    'session.timeout': 3600 * 24, # 1 day
    'session.type': 'cookie',
    'session.validate_key': True,
}
app = SessionMiddleware(app, session_opts)

UPLOAD_DIR="/home/kumu/Uploads/"

def post_get(name, default=''):
    return bottle.request.POST.get(name, default).strip()
con = mdb.connect('localhost', 'root', 'tfowifd', 'goshidb')
with con:
  cur = con.cursor()
  cur.execute("DROP TABLE IF EXISTS Units")
  cur.execute("DROP TABLE IF EXISTS Bom")
  cur.execute("DROP TABLE IF EXISTS Netlist")
  cur.execute("DROP TABLE IF EXISTS Basic")
  cur.execute("CREATE TABLE Basic(pnid INT PRIMARY KEY AUTO_INCREMENT,Arena VARCHAR(25) UNIQUE, CreateDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP) ENGINE = INNODB;")
  cur.execute("CREATE TABLE Units(bid INT PRIMARY KEY AUTO_INCREMENT, Basic_pnid INT, SerialNumber VARCHAR(25), BuildNumber VARCHAR(25), CurrentDate TIMESTAMP DEFAULT CURRENT_TIMESTAMP)ENGINE = INNODB;")
  cur.execute("CREATE TABLE Bom(bomid INT PRIMARY KEY AUTO_INCREMENT,Basic_pnid INT, Value TEXT, RefDes TEXT)ENGINE = INNODB;")
  cur.execute("CREATE TABLE Netlist(Netid INT PRIMARY KEY AUTO_INCREMENT, Basic_pnid INT, Net TEXT, RefId TEXT, Pin TEXT, Name TEXT)ENGINE = INNODB;")
@bottle.route('/')
@bottle.route('/home')
def home():
    return """
    <html>
<head>
	<title></title>
</head>
<body>
<form action="/create" method="get">
<p style="text-align: center;"><input name="Create" style="height:150px; width:300px" type="submit" value="Create" /></p>
</form>
<p style="text-align: center;"><input name="Change" style="height:150px; width:300px" type="button" value="Change" /></p>

<p style="text-align: center;"><input name="Check" style="height:150px; width:300px" type="button" value="Check" /></p>
</body>
</html>

"""
@bottle.route('/error')
def error():
    return '''
    <html>
<head>
	<title></title>
</head>
<body>
<p style="text-align: center;"><span style="font-size:48px;">Invalid Barcode</span></p>
</body>
</html>

    '''
@bottle.route('/create')
def create():
    return """
         <form action="/create" method="post">
          <input name="WholeNumber" " type="text" autofocus />
        <input value="Submit" type="submit" />
    </form>
    """

@bottle.post('/create')
def create():
    whole_number = str(post_get('WholeNumber'))
    if '830'not in whole_number[:3]:
        bottle.redirect('/error')
    else:
        arenanumber = whole_number[:9]
        serialnumber = whole_number[-4:]
        with con:
            cur.execute("INSERT INTO Basic(Arena) VALUES(%s)", arenanumber)
            cur.execute("SELECT * FROM Basic ")
            cur.execute("ALTER TABLE Units ADD CONSTRAINT FK_Units FOREIGN KEY(Basic_pnid) REFERENCES Basic(pnid) ON UPDATE CASCADE ON DELETE CASCADE;")
            cur.execute("ALTER TABLE Bom ADD CONSTRAINT FK_Bom FOREIGN KEY(Basic_pnid) REFERENCES Basic(pnid) ON UPDATE CASCADE ON DELETE CASCADE;")
            cur.execute("ALTER TABLE Netlist ADD CONSTRAINT FK_Netlist FOREIGN KEY(Basic_pnid) REFERENCES Basic(pnid) ON UPDATE CASCADE ON DELETE CASCADE;")
            cur.execute("INSERT INTO Units(SerialNumber) VALUES(%s)", serialnumber)
            cur.execute("SELECT * FROM Units")
            print cur.fetchall()
        bottle.redirect('/nextstep')

@bottle.route('/nextstep')
def nextstep():
    return """<html>
<head>
	<title></title>
</head>
<body>
<form action="/nextstep" method="post" enctype='multipart/form-data'>
<p style="text-align: center;"><span style="font-size:22px;">Build Number</span></p>

<p style="text-align: center;"><input name="BuildNumber" type="text" /></p>

<p style="text-align: center;">&nbsp;</p>

<p style="text-align: center;"><span style="font-size:22px;">Upload BOM</span></p>

<p style="text-align: center;"><input accept="MIME_type" name="BOM" type="file" /></p>

<p style="text-align: center;">&nbsp;</p>

<p style="text-align: center;">&nbsp;</p>

<p style="text-align: center;"><span style="font-size:22px;">Upload NetList</span></p>

<p style="text-align: center;"><input accept="MIME_type" name="NetList" type="file" /></p>

<p style="text-align: center;">&nbsp;</p>

<p style="text-align: center;"><input type="submit" />&nbsp;</p>
</form>
"""
@bottle.post('/nextstep')
def nextstep():
    BuildNumber = post_get('BuildNumber')
    cur.execute("INSERT INTO Units(BuildNumber) VALUES(%s)", BuildNumber)
    list_file = request.forms.get("BOM")
    Upload = request.files.get('BOM')
    Upload2 = request.files.get('NetList')
    BOM = Upload.filename
    BOM_ADDRESS = UPLOAD_DIR + BOM
    Netlist = Upload2.filename
    if Upload2 is not None:
        NET_ADDRESS  = UPLOAD_DIR + Netlist

    if '.xlsx' not in BOM:
        return "Invalid File"

    if os.path.isfile(BOM_ADDRESS) == True:
        os.remove(BOM_ADDRESS)
    Upload.save(BOM_ADDRESS)
    workbook = xlrd.open_workbook(BOM_ADDRESS)
    sheet = workbook.sheet_by_index(0)
    #If errors exist in pnid to Basic_pnid conversion they probably are here
    cur.execute("SELECT pnid FROM Basic")
    (xyz,) = cur.fetchone()
    Basic_pnid = str(xyz).strip(punct)
    print Basic_pnid
    for row in range(sheet.nrows):
         if row != 0:
                Value = sheet.cell_value(row, 2 )
                RefDes = sheet.cell_value(row,5)
                #print Value
                #print RefDes
                cur.execute("INSERT INTO Bom(Value, RefDes, Basic_pnid) Values(%s, %s, %s)", (Value, RefDes, Basic_pnid))
                cur.execute("SELECT * FROM Bom")
    print cur.fetchall()



    if os.path.isfile(NET_ADDRESS) == True:
        os.remove(NET_ADDRESS)
    Upload2.save(NET_ADDRESS)

    if Netlist[-5:] == '.xlsx':
         networkbook = xlrd.open_workbook(NET_ADDRESS)
         netsheet = networkbook.sheet_by_index(0)
          #If errors exist in pnid to Basic_pnid conversion they probably are here
         cur.execute("SELECT pnid FROM Basic")
         xyz = cur.fetchone()
         Basic_pnid = str(xyz).strip(punct)
         for rows in range(netsheet.nrows):
                if rows != 0:
                    if sheet.cell_value(rows, 0) == 'NET_NAME':
                         netname = sheet.cell_value(rows+1, 0)
                         print netname
                if sheet.cell_value(rows, 1) is not ' ':
                    nodename = sheet.cell_value(rows,1 )
                    str.split(nodename)
                    cur.execute("INSERT INTO Netlist( Net, RefId, Pin, Basic_pnid) Values(%s, %s, %s, %s))", (netname, nodename[0], nodename[1], Basic_pnid))
                    cur.execute("SELECT * FROM Netlist")
                    print cur.fetchall()
    if Netlist[-4:] == '.dat':
        line = open(NET_ADDRESS, 'r')
         #If errors exist in pnid to Basic_pnid conversion they probably are here
        cur.execute("SELECT pnid FROM Basic")
        xyz = cur.fetchone()
        Basic_pnid = str(xyz).strip(punct)
        for now in line:
            #print line
            currentline = now.rstrip()
            #print currentline
            if 'NET_NAME' in currentline:
               nename = next(line)
               if nename[-2:] == '/n':
                netname = nename[:-2]
               else:
                   netname = nename
            if 'NODE_NAME' in currentline:
                splitline = currentline.split()
                refid = splitline[1]
                pin = splitline[2]
            if currentline[-2:] == ":;":
                Name = currentline[:-2]
                cur.execute("INSERT INTO Netlist( Net, RefId, Pin, Name, Basic_pnid) Values( %s, %s, %s, %s, %s)", (netname, refid.strip('/n'), pin, Name, Basic_pnid))
        cur.execute("SELECT * FROM Netlist")
        print cur.fetchall()
bottle.run(reloader= True )
